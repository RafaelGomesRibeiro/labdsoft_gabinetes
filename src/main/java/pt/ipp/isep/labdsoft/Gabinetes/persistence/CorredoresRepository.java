package pt.ipp.isep.labdsoft.Gabinetes.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pt.ipp.isep.labdsoft.Gabinetes.domain.Corredor;

import java.util.Optional;

public interface CorredoresRepository extends JpaRepository<Corredor,Long> {

    public Optional<Corredor> findFirstByNome(String nome);

    public Optional<Corredor> findFirstByNext_Nome(String nome);

    public Optional<Corredor> findFirstByGabinete_IdGabinete(String idGabinete);

    public Optional<Corredor> findFirstByUtenteID(Long utenteId);

    Boolean existsByUtenteIDAndAndGabinete_IdGabinete(Long utenteId, String GabineteId);

    boolean existsByUtenteID(Long utenteID);
}
