package pt.ipp.isep.labdsoft.Gabinetes.services.rabbitMQ.listener;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class EquipaMedicaAlocadaEvent {
    private String dataInicio;
    private String dataFim;
    private String utenteId;
    private String medicoId;
    private String psicologoId;
}
