package pt.ipp.isep.labdsoft.Gabinetes.services.rabbitMQ.listener;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ConsultaMarcadaEvent {
    private String inicio;
    private String fim;
    private String utenteId;
}
