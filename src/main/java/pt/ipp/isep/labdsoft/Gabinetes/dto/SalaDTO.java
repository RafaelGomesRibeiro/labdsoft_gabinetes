package pt.ipp.isep.labdsoft.Gabinetes.dto;

public class SalaDTO {
    public String nome;
    public String utente;

    public SalaDTO(String nome, String utente) {
        this.nome = nome;
        this.utente = utente;
    }
}
