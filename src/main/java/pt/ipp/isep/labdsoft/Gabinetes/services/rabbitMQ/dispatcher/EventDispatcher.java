package pt.ipp.isep.labdsoft.Gabinetes.services.rabbitMQ.dispatcher;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class EventDispatcher {
    private static final String GABINETES_EXCHANGE_KEY = "gabinetes";
    private static final String GABINETES_CUSTOM_EVENT_KEY = "A";

    private RabbitTemplate template;

    public EventDispatcher(RabbitTemplate template) {
        this.template = template;
    }

    public void dispatchGabineteAlocadoEvent(GabineteAlocadoEvent event) {
        template.convertAndSend(GABINETES_EXCHANGE_KEY, GABINETES_CUSTOM_EVENT_KEY, event);
    }

}
