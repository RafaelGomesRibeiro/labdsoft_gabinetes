package pt.ipp.isep.labdsoft.Gabinetes.services.rabbitMQ.dispatcher;

import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public final class GabineteAlocadoEvent implements Serializable {
    private String utenteId;
    private String gabineteId;
}
