package pt.ipp.isep.labdsoft.Gabinetes.services.rabbitMQ.listener;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pt.ipp.isep.labdsoft.Gabinetes.dto.ReservaDto;
import pt.ipp.isep.labdsoft.Gabinetes.persistence.ReservasRepository;
import pt.ipp.isep.labdsoft.Gabinetes.services.GabinetesService;
import pt.ipp.isep.labdsoft.Gabinetes.services.ReservasService;
import pt.ipp.isep.labdsoft.Gabinetes.services.rabbitMQ.dispatcher.EventDispatcher;
import pt.ipp.isep.labdsoft.Gabinetes.services.rabbitMQ.dispatcher.GabineteAlocadoEvent;
import pt.ipp.isep.labdsoft.Gabinetes.utils.DatesFormatter;

@Component
public final class EventListener {

    private static final String USERS_EXCHANGE_NAME = "utilizadores";
    private static final String EXCHANGE_TYPE = "direct";
    private static final String EQUIPA_MEDICA_ALOCADA_EVENT_KEY = "A";

    private static final String ESTUDO_EXCHANGE_KEY = "estudo";
    private static final String CONSULTA_MARCADA_EVENT_KEY = "A";
    private static final String CONSULTA_ATUALIZADA_EVENT_KEY = "B";
    private static final String CONSULTA_CANCELADA_EVENT_KEY = "C";


    @Autowired
    private ReservasService reservasService;


    @RabbitListener(bindings = {@QueueBinding(value = @Queue(value = ""), exchange = @Exchange(value = USERS_EXCHANGE_NAME, type = EXCHANGE_TYPE), key = EQUIPA_MEDICA_ALOCADA_EVENT_KEY)})
    public void listenEquipaMedicaAlocadaEvent(EquipaMedicaAlocadaEvent event) {
        handleEquipaMedicaAlocadaEvent(event);
    }

    @RabbitListener(bindings = {@QueueBinding(value = @Queue(value = ""), exchange = @Exchange(value = ESTUDO_EXCHANGE_KEY, type = EXCHANGE_TYPE), key = CONSULTA_MARCADA_EVENT_KEY)})
    public void listenConsultaMarcadaEvent(ConsultaMarcadaEvent event) {
        handleConsultaMarcadaEvent(event);
    }

    @RabbitListener(bindings = {@QueueBinding(value = @Queue(value = ""), exchange = @Exchange(value = ESTUDO_EXCHANGE_KEY, type = EXCHANGE_TYPE), key = CONSULTA_ATUALIZADA_EVENT_KEY)})
    public void listenConsultaAtualizadaEvent(ConsultaAtualizadaEvent event) {
        reservasService.atualizarReserva(event.getInicioAntigo(), event.getFimAntigo(), event.getInicioNovo(), event.getFimNovo(), event.getUtenteId());
    }

    @RabbitListener(bindings = {@QueueBinding(value = @Queue(value = ""), exchange = @Exchange(value = ESTUDO_EXCHANGE_KEY, type = EXCHANGE_TYPE), key = CONSULTA_CANCELADA_EVENT_KEY)})
    public void listenConsultaCanceladaEvent(ConsultaCanceladaEvent event) {
        reservasService.cancelarReserva(event.getInicio(), event.getFim(), event.getUtenteId());
    }

    private void handleEquipaMedicaAlocadaEvent(EquipaMedicaAlocadaEvent event) {
        reservasService.createReserva(DatesFormatter.convertToLocalDateTime(event.getDataInicio()),
                DatesFormatter.convertToLocalDateTime(event.getDataFim()), Long.parseLong(event.getUtenteId()));
    }

    private void handleConsultaMarcadaEvent(ConsultaMarcadaEvent event) {
        reservasService.createReserva(DatesFormatter.convertToLocalDateTime(event.getInicio()),
                DatesFormatter.convertToLocalDateTime(event.getFim()), Long.parseLong(event.getUtenteId()));
    }


}
