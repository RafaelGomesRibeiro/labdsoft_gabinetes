package pt.ipp.isep.labdsoft.Gabinetes.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.ipp.isep.labdsoft.Gabinetes.domain.Corredor;
import pt.ipp.isep.labdsoft.Gabinetes.dto.GabineteDto;
import pt.ipp.isep.labdsoft.Gabinetes.dto.SalaDTO;
import pt.ipp.isep.labdsoft.Gabinetes.services.PlantaService;

import java.util.List;

@RestController
@RequestMapping("planta")
public class PlantaController {

    @Autowired
    PlantaService plantaService;

    @GetMapping("/")
    public ResponseEntity<List<SalaDTO>> getAll(){
        return new ResponseEntity<List<SalaDTO>>(plantaService.getMapaDePanico(), HttpStatus.OK);
    }

    @GetMapping("/entrar/{corredorId}/{utenteId}")
    public ResponseEntity<Corredor> entrarCorredor(@PathVariable String corredorId,@PathVariable Long utenteId){
        return new ResponseEntity<Corredor>(plantaService.entrarCorredor(corredorId,utenteId), HttpStatus.OK);
    }

    @GetMapping("/saida/{utenteId}")
    public ResponseEntity<Boolean> saida(@PathVariable Long utenteId){
        return new ResponseEntity<Boolean>(plantaService.sairClinica(utenteId),HttpStatus.OK);
    }

}
