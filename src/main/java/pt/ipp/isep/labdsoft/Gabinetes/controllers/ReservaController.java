package pt.ipp.isep.labdsoft.Gabinetes.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.ipp.isep.labdsoft.Gabinetes.dto.SalaDTO;
import pt.ipp.isep.labdsoft.Gabinetes.services.ReservasService;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("reservas")
public class ReservaController {

    @Autowired
    private ReservasService service;

    @GetMapping("/disponibilidadeUtente")
    public ResponseEntity<Boolean> disponibilidadeUtente(@RequestParam("id")Long id, @RequestParam("data")String data){
        return new ResponseEntity<Boolean>(service.disponibilidadeUtente(id,data), HttpStatus.OK);
    }
}
