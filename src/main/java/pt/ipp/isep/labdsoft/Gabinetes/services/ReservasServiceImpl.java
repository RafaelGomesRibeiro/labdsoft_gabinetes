package pt.ipp.isep.labdsoft.Gabinetes.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ipp.isep.labdsoft.Gabinetes.domain.EstadoReserva;
import pt.ipp.isep.labdsoft.Gabinetes.domain.Reserva;
import pt.ipp.isep.labdsoft.Gabinetes.persistence.ReservasRepository;
import pt.ipp.isep.labdsoft.Gabinetes.utils.DatesFormatter;

import java.time.LocalDateTime;

@Service
public class ReservasServiceImpl implements ReservasService {

    @Autowired
    private ReservasRepository repository;

    @Override
    public void createReserva(LocalDateTime dataInicio, LocalDateTime dataFim, Long utenteId) {
        Reserva reserva = new Reserva(dataInicio, dataFim, null, utenteId);
        repository.save(reserva);
    }

    @Override
    public Boolean disponibilidadeUtente(Long id, String data) {
        final Reserva reserva = repository.findFirstByUtenteIDAndDataInicioAndEstadoReservaNot(id, DatesFormatter.convertToLocalDateTime(data), EstadoReserva.VENCIDA).orElse(null);
        return reserva == null || reserva.isVencida();
    }

    @Override
    public void atualizarReserva(String inicioAntigo, String fimAntigo, String inicioNovo, String fimNovo, String utenteId) {
        final Reserva reserva = repository.findFirstByUtenteIDAndDataInicioAndEstadoReservaNot(Long.parseLong(utenteId), DatesFormatter.convertToLocalDateTime(inicioAntigo), EstadoReserva.VENCIDA).orElse(null);
        if (reserva != null && reserva.isIniciada()) { // só se pode atualizar reservas que estão confirmadas
            reserva.setDataInicio(DatesFormatter.convertToLocalDateTime(inicioNovo));
            reserva.setDataFim(DatesFormatter.convertToLocalDateTime(fimNovo));
            repository.save(reserva);
        }
    }

    @Override
    public void cancelarReserva(String inicio, String fim, String utenteId) {
        final Reserva reserva = repository.findFirstByUtenteIDAndDataInicioAndEstadoReservaNot(Long.parseLong(utenteId), DatesFormatter.convertToLocalDateTime(inicio), EstadoReserva.VENCIDA).orElse(null);
        if (reserva != null && reserva.isIniciada()) { // só se pode cancelar reservas que estão confirmadas
            reserva.vencer();
            repository.save(reserva);
        }
    }
}
