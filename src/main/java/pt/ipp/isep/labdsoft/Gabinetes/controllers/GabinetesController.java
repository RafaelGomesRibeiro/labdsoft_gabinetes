package pt.ipp.isep.labdsoft.Gabinetes.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.ipp.isep.labdsoft.Gabinetes.dto.GabineteDto;
import pt.ipp.isep.labdsoft.Gabinetes.services.GabinetesService;

import java.util.List;

@RestController
@RequestMapping("gabinetes")
public final class GabinetesController {

    @Autowired
    private GabinetesService gabinetesService;

    @PostMapping("/")
    public ResponseEntity<GabineteDto> Create(@RequestBody GabineteDto dto){
        GabineteDto gabineteCriado = gabinetesService.Save(dto);
        return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON).body(gabineteCriado);
    }

    @GetMapping("/count")
    public ResponseEntity<Integer> countGabinetes(){
        return new ResponseEntity<Integer>(gabinetesService.getNumberOfGabinetes(),HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<List<GabineteDto>> getAll(){
        return new ResponseEntity<List<GabineteDto>>(gabinetesService.listAll(),HttpStatus.OK);
    }

    @GetMapping("/aOcupar/{idUtente}")
    public  ResponseEntity<List<String>> getGabineteParaOcupar(@PathVariable Long idUtente){
        return new ResponseEntity<List<String>>(gabinetesService.getGabineteParaOcupar(idUtente),HttpStatus.OK);
    }

    @GetMapping("/entrada/{idGabinete}/{idUtente}")
    public ResponseEntity<Boolean> entradaGabinete(@PathVariable String idGabinete,@PathVariable Long idUtente){
        return new ResponseEntity<Boolean>(gabinetesService.entradaGabinete(idUtente,idGabinete),HttpStatus.OK);
    }

    @GetMapping("/saida/{idGabinete}/{idUtente}")
    public ResponseEntity<Boolean> saidaGabinete(@PathVariable String idGabinete,@PathVariable Long idUtente){
        return new ResponseEntity<Boolean>(gabinetesService.saidaGabinete(idUtente,idGabinete),HttpStatus.OK);
    }
}
