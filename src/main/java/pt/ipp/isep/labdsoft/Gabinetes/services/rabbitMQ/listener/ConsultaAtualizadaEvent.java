package pt.ipp.isep.labdsoft.Gabinetes.services.rabbitMQ.listener;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ConsultaAtualizadaEvent {
    private String inicioAntigo;
    private String fimAntigo;
    private String inicioNovo;
    private String fimNovo;
    private String utenteId;
}
