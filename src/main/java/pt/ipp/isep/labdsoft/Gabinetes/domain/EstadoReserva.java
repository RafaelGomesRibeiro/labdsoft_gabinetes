package pt.ipp.isep.labdsoft.Gabinetes.domain;

public enum EstadoReserva {
    INICIADA {
        @Override
        public String toString() {
            return "Iniciada";
        }
    },
    CONFIRMADA {
        @Override
        public String toString() {
            return "Confirmada";
        }
    },
    ANDAMENTO{
        @Override
        public String toString() {
            return "Andamento";
        }
    },
    VENCIDA {
        @Override
        public String toString() {
            return "Vencida";
        }
    }
}

