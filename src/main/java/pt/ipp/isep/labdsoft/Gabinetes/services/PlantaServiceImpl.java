package pt.ipp.isep.labdsoft.Gabinetes.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ipp.isep.labdsoft.Gabinetes.domain.Corredor;
import pt.ipp.isep.labdsoft.Gabinetes.domain.EstadoReserva;
import pt.ipp.isep.labdsoft.Gabinetes.domain.Reserva;
import pt.ipp.isep.labdsoft.Gabinetes.dto.SalaDTO;
import pt.ipp.isep.labdsoft.Gabinetes.persistence.CorredoresRepository;
import pt.ipp.isep.labdsoft.Gabinetes.persistence.ReservasRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class PlantaServiceImpl implements PlantaService {

    @Autowired
    ReservasRepository reservasRepository;
    @Autowired
    CorredoresRepository corredoresRepository;

    @Override
    public List<SalaDTO> getMapaDePanico() {
        List<SalaDTO> salas = new ArrayList<>();
        List<Corredor> corrs = corredoresRepository.findAll();
        for (Corredor c:corrs) {
            if(c.getUtenteID() != -1L) salas.add(new SalaDTO(c.getNome(),c.getUtenteID().toString()));
        }

        List<Reserva> reservas = reservasRepository.findAllByEstadoReserva(EstadoReserva.ANDAMENTO).orElse(new ArrayList<>());
        for (Reserva r:reservas) {
            salas.add(new SalaDTO(r.getGabinete().getIdGabinete(),r.getUtenteID().toString()));
        }
        return salas;
    }

    @Override
    public Corredor entrarCorredor(String corredorId, Long utenteId) {

        Corredor c = corredoresRepository.findFirstByNome(corredorId).orElseThrow(()-> new NoSuchElementException("Corredor não existe"));
        if(corredorId.equals("C1") && c.getUtenteID().equals(-1L) ){

            Reserva res = reservasRepository.findFirstByUtenteIDAndEstadoReserva(utenteId, EstadoReserva.CONFIRMADA).orElseThrow(()->new NoSuchElementException("Erro: Nao tem reserva"));
            if(reservasRepository.existsReservaByEstadoReservaAndGabinete_IdGabinete(EstadoReserva.ANDAMENTO,res.getGabinete().getIdGabinete())) throw new IllegalArgumentException("Por favor espere pelo termino da consulta pendente no gabinete atribuido");
            List<Reserva> reservas = reservasRepository.findAllByEstadoReservaAndGabinete_IdGabinete(EstadoReserva.CONFIRMADA,res.getGabinete().getIdGabinete());
            for(Reserva rAux : reservas){
                if(corredoresRepository.existsByUtenteID(rAux.getUtenteID())) throw new IllegalArgumentException("Por favor espere pela disponibilidade do gabinete");
            }


            c.setUtenteID(utenteId);
            c = corredoresRepository.save(c);
            return c;
        }else {
            Corredor anterior = corredoresRepository.findFirstByNext_Nome(corredorId).orElseThrow(() -> new NoSuchElementException("O Corredor atual não tem corredores anteriores"));
            if (!anterior.getUtenteID().equals(utenteId)) {
                throw new IllegalArgumentException("Nao pode entrar num corredor onde nao tem acesso");
            }else if(!c.getUtenteID().equals(utenteId) && !c.getUtenteID().equals(-1L)){
                throw new IllegalArgumentException("Nao pode entrar num corredor oucupado");

            }else if(anterior.getGabinete()==null){
                // TODO MUDAR PARA TER CONFIRMADA OU EM ANDAMENTO FALHA
                Reserva r = reservasRepository.findFirstByUtenteIDAndEstadoReserva(utenteId,EstadoReserva.VENCIDA)
                        .orElseThrow(()->new IllegalArgumentException("Utente nao tem reserva vencida"));
               // if(r.getGabinete().getIdGabinete().equals(anterior.getGabinete().getIdGabinete())) throw new IllegalArgumentException("Lamentamos, mas não está autorizado a aceder a esta divisao");
                if(!anterior.getNext().getNome().equals(c.getNome())) throw new IllegalArgumentException("Lamentamos, mas não está autorizado a aceder a esta divisao");
                c.setUtenteID(utenteId);
                anterior.setUtenteID(-1L);
                corredoresRepository.save(anterior);
                corredoresRepository.save(c);
                return c;
            }else{
                Reserva r = reservasRepository.findFirstByUtenteIDAndEstadoReserva(utenteId,EstadoReserva.CONFIRMADA)
                      .orElseThrow(()->new IllegalArgumentException("Utente nao tem reserva"));
                System.out.println(r);
                if(r.getGabinete().getIdGabinete().equals(anterior.getGabinete().getIdGabinete())) throw new IllegalArgumentException("Lamentamos, mas não está autorizado a aceder a esta divisao");
                c.setUtenteID(utenteId);
                anterior.setUtenteID(-1L);
                corredoresRepository.save(anterior);
                corredoresRepository.save(c);
                return c;

            }
        }
    }


    @Override
    public Boolean sairClinica(Long utenteId){
        Corredor saida = corredoresRepository.findFirstByUtenteID(utenteId).orElseThrow(()->new IllegalArgumentException("Utente deve estar na saida"));
        if(saida.getNext() == null){
            saida.setUtenteID(-1L);
            corredoresRepository.save(saida);
            return true;
        }
        throw new IllegalArgumentException("nao e possivel sair daqui");
    }
}

