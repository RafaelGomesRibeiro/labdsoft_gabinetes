package pt.ipp.isep.labdsoft.Gabinetes.services;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public final class ErrorDetails {
    private String errorMessage;
    private int statusCode;
}
