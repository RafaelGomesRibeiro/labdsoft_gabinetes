package pt.ipp.isep.labdsoft.Gabinetes.persistence;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Gabinetes.domain.EstadoGabinete;
import pt.ipp.isep.labdsoft.Gabinetes.domain.EstadoReserva;
import pt.ipp.isep.labdsoft.Gabinetes.domain.Gabinete;
import pt.ipp.isep.labdsoft.Gabinetes.domain.Reserva;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ReservasRepository extends JpaRepository<Reserva, Long> {
    @Query(value = "SELECT * FROM reserva r WHERE r.utenteid = ?1 AND r.estado_reserva = 0 AND r.gabinete_id is null ORDER BY r.data_inicio",nativeQuery = true)
    List<Reserva> findFirstFreeByUtenteID(Long utenteId);

  //  @Query(value="SELECT DISTINCT r.gabinete_id FROM reserva r WHERE r.data_inicio = ?1 AND r.vencida = false", nativeQuery = true)
  //  Optional<List<Long>> findUsedGabinetesByTime(LocalDateTime dataInicio);

    Optional<List<Reserva>> findAllByDataInicioAndEstadoReserva(LocalDateTime dataIn, EstadoReserva vencida);

    @Query("select r FROM Reserva r where r.gabinete = ?1 AND r.estadoReserva = ?2 order by r.dataInicio")
    Optional<List<Reserva>> findReservaByGabineteAndEstadoReserva(Gabinete g, EstadoReserva estado);

    @Query("SELECT case when count(r) > 0 then true else false end FROM Reserva r WHERE r.estadoReserva = ?2 AND r.utenteID = ?1")
    Boolean existsReservaByUtenteIDAndEstadoReserva(Long utenteId, EstadoReserva estado);

    @Query("SELECT case when count(r) > 0 then true else false end FROM Reserva r WHERE r.estadoReserva = ?2 AND r.utenteID = ?1 AND r.gabinete = ?3")
    Boolean existsReservaByUtenteIDAndEstadoReservaAndGabinente(Long utenteId, EstadoReserva estado, Gabinete g);


    Optional<Reserva> findFirstByUtenteIDAndEstadoReserva(Long utenteId, EstadoReserva estadoReserva);

    Optional<Reserva> findFirstByUtenteIDAndEstadoReservaAndGabinete(Long utenteId, EstadoReserva estadoReserva, Gabinete gabinete);

    Optional<List<Reserva>> findAllByEstadoReserva(EstadoReserva estadoReserva);

    List<Reserva> findAllByEstadoReservaAndGabinete_IdGabinete(EstadoReserva estado, String gabineteId);

    Boolean existsReservaByEstadoReservaAndGabinete_IdGabinete(EstadoReserva estado, String gabineteId);

    Optional<Reserva> findFirstByUtenteIDAndDataInicioAndEstadoReservaNot(Long utenteId, LocalDateTime data, EstadoReserva estadoReserva);
}
