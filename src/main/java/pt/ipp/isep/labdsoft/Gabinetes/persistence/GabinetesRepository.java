package pt.ipp.isep.labdsoft.Gabinetes.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Gabinetes.domain.EstadoGabinete;
import pt.ipp.isep.labdsoft.Gabinetes.domain.Gabinete;

import java.util.List;
import java.util.Optional;

@Repository
public interface GabinetesRepository extends JpaRepository<Gabinete, Long> {
    List<Gabinete> findGabineteByEstado(EstadoGabinete estadoGabinete);

    @Query("select g from Gabinete g WHERE NOT(g.id IN (?1))")
    Optional<List<Gabinete>> findAvailablegabinetes( List<Long> nomesGabinetesUsados);

    Optional<Gabinete> findFirstByIdGabinete(String idGabinete);

}
