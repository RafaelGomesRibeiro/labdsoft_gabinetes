package pt.ipp.isep.labdsoft.Gabinetes.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
public final class ReservaDto {
    public LocalDateTime dataInicio;
    public LocalDateTime dataFim;
    public GabineteDto gabinete;
    public Long utenteID;
}
