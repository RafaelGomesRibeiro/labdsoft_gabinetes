package pt.ipp.isep.labdsoft.Gabinetes.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.ipp.isep.labdsoft.Gabinetes.dto.GabineteDto;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public final class Gabinete {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true)
    private String idGabinete;
    private EstadoGabinete estado;
    @OneToOne
    private Corredor saida;

    public Gabinete(String idGabinete, EstadoGabinete estado, Corredor saida) {
        this.idGabinete = idGabinete;
        this.estado = estado;
        this.saida = saida;
    }



    public static Gabinete fromDto(GabineteDto dto) {
        EstadoGabinete estado;
        if (dto.estado == null) {
            estado = EstadoGabinete.LIVRE;
        } else {
            estado = EstadoGabinete.valueOf(dto.estado.toUpperCase());
        }
        return new Gabinete(dto.idGabinete, estado, dto.saida);
    }

    public GabineteDto toDto() {
        return new GabineteDto(idGabinete, estado.toString(),saida);
    }
}
