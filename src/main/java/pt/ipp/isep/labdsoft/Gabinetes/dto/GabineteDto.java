package pt.ipp.isep.labdsoft.Gabinetes.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import pt.ipp.isep.labdsoft.Gabinetes.domain.Corredor;

@AllArgsConstructor
@NoArgsConstructor
public final class GabineteDto {
    public String idGabinete;
    public String estado;
    public Corredor saida;
}
