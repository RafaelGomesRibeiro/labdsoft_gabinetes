package pt.ipp.isep.labdsoft.Gabinetes.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ipp.isep.labdsoft.Gabinetes.domain.*;
import pt.ipp.isep.labdsoft.Gabinetes.dto.GabineteDto;
import pt.ipp.isep.labdsoft.Gabinetes.persistence.CorredoresRepository;
import pt.ipp.isep.labdsoft.Gabinetes.persistence.GabinetesRepository;
import pt.ipp.isep.labdsoft.Gabinetes.persistence.ReservasRepository;
import pt.ipp.isep.labdsoft.Gabinetes.services.rabbitMQ.dispatcher.EventDispatcher;
import pt.ipp.isep.labdsoft.Gabinetes.services.rabbitMQ.dispatcher.GabineteAlocadoEvent;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
@Transactional
public class GabinetesServiceImpl implements GabinetesService {

    private static final String FIRST_CORREDOR = "C1";

    @Autowired
    private GabinetesRepository repository;

    @Autowired
    private EventDispatcher dispatcher;

    @Autowired
    private ReservasRepository reservaRepo;

    @Autowired
    private CorredoresRepository corredoresRepository;

    @Override
    public GabineteDto Save(GabineteDto dto) {
        Gabinete g = Gabinete.fromDto(dto);
        return repository.save(g).toDto();
    }

    @Override
    public List<GabineteDto> listAll() {
        return repository.findAll().stream().map(Gabinete::toDto).collect(Collectors.toList());
    }

    @Override
    public Integer getNumberOfGabinetes() {
        return repository.findAll().size();
    }

    @Override
    public List<String> getGabineteParaOcupar(Long idUtente) {
        // VERIFICAR SE JA CONFIRMOU ALGUMA RESERVA
        if (reservaRepo.existsReservaByUtenteIDAndEstadoReserva(idUtente, EstadoReserva.CONFIRMADA))
            throw new IllegalArgumentException("Por Não pode comparecer a marcações em simultaneo, trate os assuntos pendentes e tente de novo");
        List<Reserva> lR = reservaRepo.findFirstFreeByUtenteID(idUtente);
        // PROCURAR RESERVA
        System.out.println(lR);
        if (lR.isEmpty())
            throw new NoSuchElementException("Nao Tem autorização para aceder a gabinetes: Marque uma Consulta");
        Reserva r = lR.get(0);

        // PESQUISAR GABINETES USADOS NESTA HORA
        List<Reserva> reservasIntercetadas = reservaRepo.findAllByDataInicioAndEstadoReserva(r.getDataInicio(), EstadoReserva.CONFIRMADA).orElse(new ArrayList<>());
        List<Long> gabinetesUsados = new ArrayList<>();
        for (Reserva resInt : reservasIntercetadas) {
            //TODO COLOCAR ISTO NUMA QUERY
            if (resInt.getGabinete() != null) {
                if (resInt.getGabinete().getId() != null) {
                    gabinetesUsados.add(resInt.getGabinete().getId());
                }
            }
        }
        Gabinete gabinete;
        System.out.println(gabinetesUsados);
        if (!gabinetesUsados.isEmpty()) {

            gabinete = repository.findAvailablegabinetes(gabinetesUsados).orElseThrow(() -> new NoSuchElementException("Não Há Gabinetes Desponiveis")).get(0);
        } else {
            gabinete = repository.findAll().get(0);
        }
        // Verificar se o gabinete ja esta oucupado
        // TODO
        // ATRIBUIR GABINETE E CONFIRMAR RESERVA
        r.setGabinete(gabinete);
        r.setEstadoReserva(EstadoReserva.CONFIRMADA);
        System.out.println(r);
        reservaRepo.save(r);
        dispatcher.dispatchGabineteAlocadoEvent(new GabineteAlocadoEvent(String.valueOf(idUtente), gabinete.getIdGabinete()));
        return getCaminhoDoGabinete(gabinete.getIdGabinete());
    }



    @Override
    public Boolean entradaGabinete(Long idUtente, String idGabinete) {
        Gabinete gab = findValidGabinete(idGabinete, "Ocupado");
        Corredor before = corredoresRepository.findFirstByGabinete_IdGabinete(idGabinete).orElseThrow(()->new IllegalArgumentException("Não foi encontrado corredor para este gabinete"));
        if(!before.getUtenteID().equals(idUtente)) throw new IllegalArgumentException("Não pode aceder a um gabinete onde não se encontra");
        before.setUtenteID(-1L);
        corredoresRepository.save(before);
        Reserva r = reservaRepo.findFirstByUtenteIDAndEstadoReservaAndGabinete(idUtente, EstadoReserva.CONFIRMADA, gab).orElseThrow(() -> new IllegalArgumentException("Nao tem o gabinete Reservado"));
        gab.setEstado(EstadoGabinete.OCUPADO);
        repository.save(gab);
        r.setEstadoReserva(EstadoReserva.ANDAMENTO);
        reservaRepo.save(r);
        return true;
    }

    @Override
    public Boolean saidaGabinete(Long idUtente, String idGabinete) {
        Gabinete gab = findValidGabinete(idGabinete, "Livre");
        if(!gab.getSaida().getUtenteID().equals(-1L)) throw new IllegalArgumentException("Não pode sair do gabinete enquanto o corredor estiver oucupado, agrarde porfavor...");
        Corredor c = gab.getSaida();
        c.setUtenteID(idUtente);
        corredoresRepository.save(c);
        Reserva res = reservaRepo.findFirstByUtenteIDAndEstadoReservaAndGabinete(idUtente, EstadoReserva.ANDAMENTO, gab).orElseThrow(() -> new IllegalArgumentException("Só pode sair de um gabinete após ordem de entrada no mesmo."));
        res.setEstadoReserva(EstadoReserva.VENCIDA);
        reservaRepo.save(res);
        gab.setEstado(EstadoGabinete.LIVRE);

        repository.save(gab);




        return true;
    }

    private Gabinete findValidGabinete(String idGabinete, String estado) {
        Gabinete g = repository.findFirstByIdGabinete(idGabinete).orElseThrow(() -> new NoSuchElementException("Gabinete " + idGabinete + " Inexistente"));
        if (g.getEstado().toString().equals(estado))
            throw new IllegalArgumentException("Gabinete " + idGabinete + " " + estado + ": não é possível realizar a operação");
        return g;
    }


    private List<String> getCaminhoDoGabinete(String idGabinete){
        Corredor first = corredoresRepository.findFirstByNome(FIRST_CORREDOR).orElseThrow(()->new NoSuchElementException("Não existe uma planta"));
        List<String> list = new ArrayList<>();

        return findPath(first, idGabinete, list);
    }

    private List<String> findPath(Corredor first, String idGabinete, List<String> list) {
        if(first == null) throw new IllegalArgumentException("Planta inválida");
        if(first.getGabinete().getIdGabinete().equalsIgnoreCase(idGabinete)){
            if(!list.add(first.getNome())) throw new IllegalArgumentException("ERRO na criação do caminho!");
            if(!list.add(idGabinete)) throw new IllegalArgumentException("ERRO na criação do caminho!!");
            return list;
        }else{
            if(!list.add(first.getNome())) throw new IllegalArgumentException("ERRO na criação do caminho!!!");
            return findPath(first.getNext(),idGabinete, list);
        }

    }


}
