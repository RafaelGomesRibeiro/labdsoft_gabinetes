package pt.ipp.isep.labdsoft.Gabinetes.services;

import pt.ipp.isep.labdsoft.Gabinetes.dto.GabineteDto;

import java.util.List;

public interface GabinetesService {
    GabineteDto Save(GabineteDto dto);

    List<GabineteDto> listAll();

    Integer getNumberOfGabinetes();

    List<String> getGabineteParaOcupar(Long idUtente);

    Boolean entradaGabinete(Long idUtente, String idGabinete);

    Boolean saidaGabinete(Long idUtente, String idGabinete);
}
