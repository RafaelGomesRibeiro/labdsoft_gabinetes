package pt.ipp.isep.labdsoft.Gabinetes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GabinetesApplication {

	public static void main(String[] args) {
		SpringApplication.run(GabinetesApplication.class, args);
	}

}
