package pt.ipp.isep.labdsoft.Gabinetes;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import pt.ipp.isep.labdsoft.Gabinetes.controllers.GabinetesController;
import pt.ipp.isep.labdsoft.Gabinetes.domain.Corredor;
import pt.ipp.isep.labdsoft.Gabinetes.domain.EstadoGabinete;
import pt.ipp.isep.labdsoft.Gabinetes.dto.GabineteDto;
import pt.ipp.isep.labdsoft.Gabinetes.persistence.CorredoresRepository;
import pt.ipp.isep.labdsoft.Gabinetes.persistence.GabinetesRepository;

import static pt.ipp.isep.labdsoft.Gabinetes.domain.EstadoGabinete.LIVRE;

@Component
public class GabinetesBootstrapper implements ApplicationRunner {

    private GabinetesController gabinetesController;
    private GabinetesRepository gabinetesRepository;
    private CorredoresRepository corredoresRepository;

    public GabinetesBootstrapper(GabinetesController gabinetesController, GabinetesRepository gabinetesRepository, CorredoresRepository corredoresRepository){
        this.gabinetesRepository = gabinetesRepository;
        this.gabinetesController = gabinetesController;
        this.corredoresRepository = corredoresRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {


        Corredor c6 = new Corredor("C6",null,null);
        c6 = corredoresRepository.save(c6);

        Corredor c5 = new Corredor("C5", c6,null );
        c5 = corredoresRepository.save(c5);

        Corredor c4 = new Corredor("C4", c5,null );
        c4 = corredoresRepository.save(c4);

        gabinetesController.Create(new GabineteDto("A1", "LIVRE",c6));
        gabinetesController.Create(new GabineteDto("A2", "LIVRE",c5));
        gabinetesController.Create(new GabineteDto("A3", "LIVRE",c4));

        Corredor c3 = new Corredor("C3",null,gabinetesRepository.findFirstByIdGabinete("A3").orElseGet(null));
        c3 = corredoresRepository.save(c3);

        Corredor c2 = new Corredor("C2",c3,gabinetesRepository.findFirstByIdGabinete("A2").orElseGet(null));
        c2 = corredoresRepository.save(c2);

        Corredor c1 = new Corredor("C1",c2,gabinetesRepository.findFirstByIdGabinete("A1").orElseGet(null));
        c1 = corredoresRepository.save(c1);





    }
}
