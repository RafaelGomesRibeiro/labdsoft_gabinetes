package pt.ipp.isep.labdsoft.Gabinetes.domain;

public enum EstadoGabinete {
    OCUPADO {
        @Override
        public String toString() {
            return "Ocupado";
        }
    },
    LIVRE {
        @Override
        public String toString() {
            return "Livre";
        }
    }
}
