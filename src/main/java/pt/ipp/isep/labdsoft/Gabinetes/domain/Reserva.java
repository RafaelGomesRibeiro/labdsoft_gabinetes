package pt.ipp.isep.labdsoft.Gabinetes.domain;

import lombok.*;
import org.hibernate.annotations.Fetch;
import pt.ipp.isep.labdsoft.Gabinetes.dto.ReservaDto;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public final class Reserva {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private LocalDateTime dataInicio;
    private LocalDateTime dataFim;
    @ManyToOne(fetch = FetchType.LAZY)
    private Gabinete gabinete;
    private Long utenteID;
    private EstadoReserva estadoReserva;

    public Reserva(LocalDateTime dataInicio, LocalDateTime dataFim, Gabinete gabinete, Long utenteID) {
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.gabinete = gabinete;
        this.utenteID = utenteID;
        this.estadoReserva = EstadoReserva.INICIADA;
    }

    public ReservaDto toDto() {
        return new ReservaDto(dataInicio, dataFim, gabinete.toDto(), utenteID);
    }

    public static Reserva fromDTO(ReservaDto dto) {
        Gabinete g = Gabinete.fromDto(dto.gabinete);
        return new Reserva(dto.dataInicio, dto.dataFim, g, dto.utenteID);
    }

    public boolean isVencida() {
        return estadoReserva == EstadoReserva.VENCIDA;
    }

    public boolean isConfirmada() {
        return estadoReserva == EstadoReserva.CONFIRMADA;
    }

    public boolean isIniciada() {
        return estadoReserva == EstadoReserva.INICIADA;
    }


    public void vencer() {
        estadoReserva = EstadoReserva.VENCIDA;
    }
}
