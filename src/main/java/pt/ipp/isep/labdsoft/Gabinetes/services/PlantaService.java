package pt.ipp.isep.labdsoft.Gabinetes.services;

import org.springframework.http.HttpStatus;
import pt.ipp.isep.labdsoft.Gabinetes.domain.Corredor;
import pt.ipp.isep.labdsoft.Gabinetes.dto.SalaDTO;

import java.util.List;

public interface PlantaService {
    List<SalaDTO> getMapaDePanico();

    Corredor entrarCorredor(String corredorId, Long utenteId);

    Boolean sairClinica(Long utenteId);
}
