package pt.ipp.isep.labdsoft.Gabinetes.services;

import pt.ipp.isep.labdsoft.Gabinetes.dto.ReservaDto;

import java.time.LocalDateTime;

public interface ReservasService {
    void createReserva(LocalDateTime dataInicio, LocalDateTime dataFim, Long utenteId);

    Boolean disponibilidadeUtente(Long id, String data);

    void atualizarReserva(String inicioAntigo, String fimAntigo, String inicioNovo, String fimNovo, String utenteId);

    void cancelarReserva(String inicio, String fim, String utenteId);
}
