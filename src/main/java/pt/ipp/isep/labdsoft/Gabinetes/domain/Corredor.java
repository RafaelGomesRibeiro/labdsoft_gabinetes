package pt.ipp.isep.labdsoft.Gabinetes.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Corredor {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long corredorId;
    @Column(unique = true)
    private String nome;
    private Long utenteID;
    @OneToOne
    private Corredor next;
    @OneToOne
    private Gabinete gabinete;

    public Corredor(String nome,Corredor next, Gabinete gabinete){
        this.nome = nome;
        this.next = next;
        this.gabinete = gabinete;
        this.utenteID = -1L;
    }

}
