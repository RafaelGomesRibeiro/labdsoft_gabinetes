package pt.ipp.isep.labdsoft.Gabinetes.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Gabinetes.dto.GabineteDto;

import java.time.LocalDateTime;

@RunWith(SpringJUnit4ClassRunner.class)
public class GabineteTests {

    @Test
    public void fromDtoTest(){
        GabineteDto gDto = new GabineteDto("a",EstadoGabinete.LIVRE.toString(),null);
        Gabinete g = Gabinete.fromDto(gDto);
        Assertions.assertEquals(gDto.estado,g.getEstado().toString());
        Assertions.assertEquals(gDto.saida,g.getSaida());
        Assertions.assertEquals(gDto.idGabinete,g.getIdGabinete());

    }

    @Test
    public void toDtoTest(){
        Reserva r = new Reserva(LocalDateTime.now(),LocalDateTime.now(),null,1L);

        Assertions.assertTrue(r.isIniciada());

    }
}
