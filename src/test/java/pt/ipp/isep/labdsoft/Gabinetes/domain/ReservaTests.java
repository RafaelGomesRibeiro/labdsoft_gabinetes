package pt.ipp.isep.labdsoft.Gabinetes.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Gabinetes.dto.GabineteDto;
import pt.ipp.isep.labdsoft.Gabinetes.dto.ReservaDto;

import java.time.LocalDateTime;

@RunWith(SpringJUnit4ClassRunner.class)

public class ReservaTests {

    @Test
    public void toDTOTest(){
        LocalDateTime in = LocalDateTime.now();
        LocalDateTime fim = in.plusMinutes(1);
        Gabinete g = new Gabinete("a",EstadoGabinete.LIVRE,null);

        Reserva r = new Reserva(in,fim,g,1L);

        ReservaDto rDto = r.toDto();

        Assertions.assertEquals(rDto.utenteID,r.getUtenteID());
        Assertions.assertEquals(rDto.dataFim,r.getDataFim());
        Assertions.assertEquals(rDto.dataInicio,r.getDataInicio());

    }

    @Test
    public void fromDto(){
        LocalDateTime in = LocalDateTime.now();
        LocalDateTime fim = in.plusMinutes(1);
        GabineteDto g = new GabineteDto("a",EstadoGabinete.LIVRE.toString(),null);

        ReservaDto rDto = new ReservaDto(in,fim,g,1L);

        Reserva r = Reserva.fromDTO(rDto);

        Assertions.assertEquals(rDto.utenteID,r.getUtenteID());
        Assertions.assertEquals(rDto.dataFim,r.getDataFim());
        Assertions.assertEquals(rDto.dataInicio,r.getDataInicio());

    }


    @Test
    public void iniciadaTest(){
        Reserva r = new Reserva(LocalDateTime.now(),LocalDateTime.now(),null,1L);

        Assertions.assertTrue(r.isIniciada());

    }

    @Test
    public void vencidaTest(){
        Reserva r = new Reserva(LocalDateTime.now(),LocalDateTime.now(),null,1L);
        r.setEstadoReserva(EstadoReserva.VENCIDA);
        Assertions.assertTrue(r.isVencida());

    }

    @Test
    public void confirmadaTest(){
        Reserva r = new Reserva(LocalDateTime.now(),LocalDateTime.now(),null,1L);
        r.setEstadoReserva(EstadoReserva.CONFIRMADA);
        Assertions.assertTrue(r.isConfirmada());

    }

}
