package pt.ipp.isep.labdsoft.Gabinetes.utils;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.junit.Assert;

import java.time.LocalDateTime;

@RunWith(SpringJUnit4ClassRunner.class)
public class DatesFormatterTest {


    @Test
    public void verificarDataNullLancaExcecao(){
       try {
           DatesFormatter.convertToString(null);
       } catch (Exception e) {
           Assert.assertEquals(NullPointerException.class, e.getClass());
       }
    }

    @Test
    public void verificarDataRetornadaTest(){
        LocalDateTime l = LocalDateTime.now();
        final String result = DatesFormatter.convertToString(l);
        Assert.assertFalse(result.isEmpty());
    }

    @Test
    public void verificarDataRetornadaTest2(){
        String dataS = "10-10-2010 10:00";
        final LocalDateTime localDateTime = DatesFormatter.convertToLocalDateTime(dataS);
        Assert.assertNotNull(localDateTime);
    }

}
